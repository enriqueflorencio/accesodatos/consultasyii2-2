<?php

use yii\helpers\Html;

$this->title = 'My Yii Application';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>Consultas 2</h1>
    </div>
    <div class="body-content">
        <div class ="col-sm-6 col-md-4">
            <div class ="thumbnail">
                <div class="caption">
                    <h3>Consulta I</h3>
                    <p>
                    <p>Número de ciclistas que hay</p>
                        <?= Html::a('Active Record', ['site/consulta1a'], ['class' => 'btn btn-primary'])?>
                        <?= Html::a('DAO', ['site/consulta1d'], ['class' => 'btn btn-success'])?>
                    </p>
                </div>
            </div>
        </div>
        <div class ="col-sm-6 col-md-4">
            <div class ="thumbnail">
                <div class="caption">
                    <h3>Consulta II</h3>
                    <p>
                         <p>Número de ciclistas que hay del equipo Banesto</p>
                        <?= Html::a('Active Record', ['site/consulta2a'], ['class' => 'btn btn-primary'])?>
                        <?= Html::a('DAO', ['site/consulta2d'], ['class' => 'btn btn-success'])?>
                    </p>
                </div>
            </div>
        </div>
        <div class ="col-sm-6 col-md-4">
            <div class ="thumbnail">
                <div class="caption">
                    <h3>Consulta III</h3>
                    <p>
                    <p>Edad media de los ciclistas</p>
                        <?= Html::a('Active Record', ['site/consulta3a'], ['class' => 'btn btn-primary'])?>
                        <?= Html::a('DAO', ['site/consulta3d'], ['class' => 'btn btn-success'])?>
                    </p>
                </div>
            </div>
        </div>
        <div class ="col-sm-6 col-md-4">
            <div class ="thumbnail">
                <div class="caption">
                    <h3>Consulta IV</h3>
                    <p>
                    <p>La edad media de los del equipo Banesto</p>
                        <?= Html::a('Active Record', ['site/consulta4a'], ['class' => 'btn btn-primary'])?>
                        <?= Html::a('DAO', ['site/consulta4d'], ['class' => 'btn btn-success'])?>
                    </p>
                </div>
            </div>
        </div>
        <div class ="col-sm-6 col-md-4">
            <div class ="thumbnail">
                <div class="caption">
                    <h3>Consulta V</h3>
                    <p>
                    <p>La edad media de los ciclistas por cada equipo</p>
                        <?= Html::a('Active Record', ['site/consulta5a'], ['class' => 'btn btn-primary'])?>
                        <?= Html::a('DAO', ['site/consulta5d'], ['class' => 'btn btn-success'])?>
                    </p>
                </div>
            </div>
        </div>
        <div class ="col-sm-6 col-md-4">
            <div class ="thumbnail">
                <div class="caption">
                    <h3>Consulta VI</h3>
                    <p>
                    <p>El número de ciclistas por equipo</p>
                        <?= Html::a('Active Record', ['site/consulta6a'], ['class' => 'btn btn-primary'])?>
                        <?= Html::a('DAO', ['site/consulta6d'], ['class' => 'btn btn-success'])?>
                    </p>
                </div>
            </div>
        </div>
        <div class ="col-sm-6 col-md-4">
            <div class ="thumbnail">
                <div class="caption">
                    <h3>Consulta VII</h3>
                    <p>
                    <p>El número total de puertos</p>
                        <?= Html::a('Active Record', ['site/consulta7a'], ['class' => 'btn btn-primary'])?>
                        <?= Html::a('DAO', ['site/consulta7d'], ['class' => 'btn btn-success'])?>
                    </p>
                </div>
            </div>
        </div>
        <div class ="col-sm-6 col-md-4">
            <div class ="thumbnail">
                <div class="caption">
                    <h3>Consulta VIII</h3>
                    <p>
                    <p>El número total de puertos mayores de 1500</p>
                        <?= Html::a('Active Record', ['site/consulta8a'], ['class' => 'btn btn-primary'])?>
                        <?= Html::a('DAO', ['site/consulta8d'], ['class' => 'btn btn-success'])?>
                    </p>
                </div>
            </div>
        </div>
        <div class ="col-sm-6 col-md-4">
            <div class ="thumbnail">
                <div class="caption">
                    <h3>Consulta IX</h3>
                    <p>
                    <p>Listar el nombre de los equipos que tengan más de 4 ciclistas</p>
                        <?= Html::a('Active Record', ['site/consulta9a'], ['class' => 'btn btn-primary'])?>
                        <?= Html::a('DAO', ['site/consulta9d'], ['class' => 'btn btn-success'])?>
                    </p>
                </div>
            </div>
        </div>
        <div class ="col-sm-6 col-md-4">
            <div class ="thumbnail">
                <div class="caption">
                    <h3>Consulta X</h3>
                    <p>
                    <p>Listar el nombre de los equipos que tengan más de 4 ciclistas cuya edad esté entre 28 y 32</p>
                        <?= Html::a('Active Record', ['site/consulta10a'], ['class' => 'btn btn-primary'])?>
                        <?= Html::a('DAO', ['site/consulta10d'], ['class' => 'btn btn-success'])?>
                    </p>
                </div>
            </div>
        </div>
        <div class ="col-sm-6 col-md-4">
            <div class ="thumbnail">
                <div class="caption">
                    <h3>Consulta XI</h3>
                    <p>
                    <p>Indícame el número de etapas que ha ganado cada uno de los ciclistas</p>
                        <?= Html::a('Active Record', ['site/consulta11a'], ['class' => 'btn btn-primary'])?>
                        <?= Html::a('DAO', ['site/consulta11d'], ['class' => 'btn btn-success'])?>
                    </p>
                </div>
            </div>
        </div>
        <div class ="col-sm-6 col-md-4">
            <div class ="thumbnail">
                <div class="caption">
                    <h3>Consulta XII</h3>
                    <p>
                    <p>Indícame el dorsal de los ciclistas que hayan ganado más de una etapa</p>
                        <?= Html::a('Active Record', ['site/consulta12a'], ['class' => 'btn btn-primary'])?>
                        <?= Html::a('DAO', ['site/consulta12d'], ['class' => 'btn btn-success'])?>
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>
