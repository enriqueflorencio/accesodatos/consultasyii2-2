<?php

namespace app\controllers;

use Yii;
use yii\data\ActiveDataProvider;
use yii\data\SqlDataProvider;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
    
    public function actionConsulta1a()
    {
        $consulta = new ActiveDataProvider ([
          'query' => \app\models\Ciclista::find()-> select("count(dorsal) as numeroCiclistas"),
          'pagination' => ['pageSize' => 1]
        ]);
        
        return $this->render("resultados",[
          "resultados" => $consulta,
          "campos" => ['numeroCiclistas'],
          "titulo" => "Consulta 1 con ActiveRecord",
          "enunciado" => "Número de ciclistas que hay",
          "sql" => "SELECT COUNT(*) FROM cilcista",
        ]);
    }
    public function actionConsulta1d() {
        $total = Yii::$app -> db
                -> createCommand('select 1')
                -> queryScalar();
        $consulta = new SqlDataProvider ([
          'sql' => 'SELECT COUNT(*) as numeroCiclista FROM ciclista',
          'totalCount' => $total,
        ]);
        
        return $this->render("resultados",[
          "resultados" => $consulta,
          "campos" => ['numeroCiclista'],
          "titulo" => "Consulta 1 con DAO",
          "enunciado" => "Número de ciclistas que hay",
          "sql" => "SELECT COUNT(*) FROM ciclista",
        ]);
    }
    
    public function actionConsulta2a()
    {
        $consulta = new ActiveDataProvider ([
          'query' => \app\models\Ciclista::find()-> where("nomequipo = 'Banesto'")-> select("count(dorsal) as numeroCiclistas"),
        ]);
        
        return $this->render("resultados",[
          "resultados" => $consulta,
          "campos" => ['numeroCiclistas'],
          "titulo" => "Consulta 2 con ActiveRecord",
          "enunciado" => "Número de ciclistas que hay del equipo Banesto",
          "sql" => "SELECT COUNT(*) FROM cilcista WHERE nomequipo = 'Banesto'",
        ]);
    }
    
     public function actionConsulta2d() {
        $total = Yii::$app -> db
                -> createCommand('select 1')
                -> queryScalar();
        $consulta = new SqlDataProvider ([
          'sql' => 'SELECT COUNT(*) as numeroCiclista FROM ciclista WHERE nomequipo = "Banesto"',
          'totalCount' => $total,
        ]);
        
        return $this->render("resultados",[
          "resultados" => $consulta,
          "campos" => ['numeroCiclista'],
          "titulo" => "Consulta 2 con DAO",
          "enunciado" => "Número de ciclistas que pertenecen a Banesto",
          "sql" => "SELECT COUNT(*) FROM ciclista WHERE nomequipo = 'Banesto'",
        ]);
    }
    
    public function actionConsulta3a()
    {
        $consulta = new ActiveDataProvider ([
          'query' => \app\models\Ciclista::find()-> select("avg(edad) as edadMedia"),
          'pagination' => ['pageSize' => 1]
        ]);
        
        return $this->render("resultados",[
          "resultados" => $consulta,
          "campos" => ['edadMedia'],
          "titulo" => "Consulta 3 con ActiveRecord",
          "enunciado" => "Edad media de los ciclistas",
          "sql" => "SELECT AVG(edad) FROM cilcista",
        ]);
    }
    
    public function actionConsulta3d() {
        $total = Yii::$app -> db
                -> createCommand('select 1')
                -> queryScalar();
        $consulta = new SqlDataProvider ([
          'sql' => 'SELECT AVG(edad) as edadMedia FROM ciclista',
          'totalCount' => $total,
        ]);
        
        return $this->render("resultados",[
          "resultados" => $consulta,
          "campos" => ['edadMedia'],
          "titulo" => "Consulta 3 con DAO",
          "enunciado" => "Edad media de los cilclistas",
          "sql" => "SELECT AVG(edad) as edadMedia FROM ciclista",
        ]);
    }
    
     public function actionConsulta4a()
    {
        $consulta = new ActiveDataProvider ([
          'query' => \app\models\Ciclista::find() -> where("nomequipo = 'Banesto'") -> select("avg(edad) as edadMedia"),
          'pagination' => ['pageSize' => 1]
        ]);
        
        return $this->render("resultados",[
          "resultados" => $consulta,
          "campos" => ['edadMedia'],
          "titulo" => "Consulta 4 con ActiveRecord",
          "enunciado" => "Edad media de los ciclistas que pertenecen a Banesto",
          "sql" => "SELECT AVG(edad) FROM cilcista WHERE nomequipo = 'Banesto'",
        ]);
    }
    
    public function actionConsulta4d() {
        $total = Yii::$app -> db
                -> createCommand('select 1')
                -> queryScalar();
        $consulta = new SqlDataProvider ([
          'sql' => 'SELECT AVG(edad) as edadMedia FROM ciclista WHERE nomequipo = "Banesto"',
          'totalCount' => $total,
        ]);
        
        return $this->render("resultados",[
          "resultados" => $consulta,
          "campos" => ['edadMedia'],
          "titulo" => "Consulta 4 con DAO",
          "enunciado" => "Edad media de los cilclistas que pertenecen a Banesto",
          "sql" => "SELECT AVG(edad) as edadMedia FROM ciclista WHERE nomequipo = 'Banesto'",
        ]);
    }
    
     public function actionConsulta5a()
    {
        $consulta = new ActiveDataProvider ([
          'query' => \app\models\Ciclista::find() -> groupBy("nomequipo") -> select("nomequipo,avg(edad) as edadMedia"),
          'pagination' => ['pageSize' => 5]
        ]);
        
        return $this->render("resultados",[
          "resultados" => $consulta,
          "campos" => ['nomequipo','edadMedia'],
          "titulo" => "Consulta 5 con ActiveRecord",
          "enunciado" => "La edad media de los ciclistas por cada equipo",
          "sql" => "SELECT nomequipo, AVG(edad) FROM cilcista GROUP BY nomequipo",
        ]);
    }
    
    public function actionConsulta5d() {
        $total = Yii::$app -> db
                -> createCommand('select count(distinct nomequipo) from ciclista')
                -> queryScalar();
        $consulta = new SqlDataProvider ([
          'sql' => 'SELECT nomequipo, AVG(edad) as edadMedia FROM ciclista GROUP BY nomequipo',
          'totalCount' => $total,
          'pagination' => ['pageSize' => 5]
        ]);
        
        return $this->render("resultados",[
          "resultados" => $consulta,
          "campos" => ['nomequipo','edadMedia'],
          "titulo" => "Consulta 5 con DAO",
          "enunciado" => "La edad media de los ciclistas por cada equipo",
          "sql" => "SELECT nomequipo, AVG(edad) as edadMedia FROM ciclista GROUP BY nomequipo",
        ]);
    }
    
     public function actionConsulta6a()
    {
        $consulta = new ActiveDataProvider ([
          'query' => \app\models\Ciclista::find() -> groupBy("nomequipo") -> select("nomequipo,count(dorsal) as numeroCiclistas"),
          'pagination' => ['pageSize' => 5]
        ]);
        
        return $this->render("resultados",[
          "resultados" => $consulta,
          "campos" => ['nomequipo','numeroCiclistas'],
          "titulo" => "Consulta 6 con ActiveRecord",
          "enunciado" => "Número de ciclistas por equipo",
          "sql" => "SELECT nomequipo, COUNT(*) FROM cilcista GROUP BY nomequipo",
        ]);
    }
    
    public function actionConsulta6d() {
        $total = Yii::$app -> db
                -> createCommand('select count(distinct nomequipo) from ciclista')
                -> queryScalar();
        $consulta = new SqlDataProvider ([
          'sql' => 'SELECT nomequipo, COUNT(dorsal) as numeroCiclistas FROM ciclista GROUP BY nomequipo',
          'totalCount' => $total,
          'pagination' => ['pageSize' => 5]
        ]);
        
        return $this->render("resultados",[
          "resultados" => $consulta,
          "campos" => ['nomequipo','numeroCiclistas'],
          "titulo" => "Consulta 6 con DAO",
          "enunciado" => "Número de ciclistas por equipo",
          "sql" => "SELECT nomequipo, COUNT(*) FROM ciclista GROUP BY nomequipo",
        ]);
    }
    
     public function actionConsulta7a()
    {
        $consulta = new ActiveDataProvider ([
          'query' => \app\models\Puerto::find() -> select("count(*) as numeroPuertos"),
        ]);
        
        return $this->render("resultados",[
          "resultados" => $consulta,
          "campos" => ['numeroPuertos'],
          "titulo" => "Consulta 7 con ActiveRecord",
          "enunciado" => "Número total de puertos",
          "sql" => "SELECT COUNT(*) FROM puerto",
        ]);
    }
    
    public function actionConsulta7d() {
        $total = Yii::$app -> db
                -> createCommand('select 1')
                -> queryScalar();
        $consulta = new SqlDataProvider ([
          'sql' => 'SELECT COUNT(*) as numeroPuertos FROM puerto',
          'totalCount' => $total,
        ]);
        
        return $this->render("resultados",[
          "resultados" => $consulta,
          "campos" => ['numeroPuertos'],
          "titulo" => "Consulta 7 con DAO",
          "enunciado" => "Número total de puertos",
          "sql" => "SELECT COUNT(*) FROM puerto",
        ]);
    }
    
     public function actionConsulta8a()
    {
        $consulta = new ActiveDataProvider ([
          'query' => \app\models\Puerto::find() ->where("altura > 1500") -> select("count(*) as numeroPuertos"),
        ]);
        
        return $this->render("resultados",[
          "resultados" => $consulta,
          "campos" => ['numeroPuertos'],
          "titulo" => "Consulta 8 con ActiveRecord",
          "enunciado" => "Número total de puertos mayores de 1500",
          "sql" => "SELECT COUNT(*) FROM puerto WHERE altura > 1500",
        ]);
    }
    
    public function actionConsulta8d() {
        $total = Yii::$app -> db
                -> createCommand('select count(*) from puerto where altura > 1500')
                -> queryScalar();
        $consulta = new SqlDataProvider ([
          'sql' => 'SELECT COUNT(*) as numeroPuertos FROM puerto WHERE altura > 1500',
          'totalCount' => $total,
        ]);
        
        return $this->render("resultados",[
          "resultados" => $consulta,
          "campos" => ['numeroPuertos'],
          "titulo" => "Consulta 8 con DAO",
          "enunciado" => "Número total de puertos mayores de 1500",
          "sql" => "SELECT COUNT(*) FROM puerto WHERE altura > 1500",
        ]);
    }
    
     public function actionConsulta9a()
    {
        $consulta = new ActiveDataProvider ([
          'query' => \app\models\Ciclista::find() -> groupBy("nomequipo") -> having("count(*) > 4") -> select("nomequipo"),
          'pagination' => ['pageSize' => 3]
        ]);
        
        return $this->render("resultados",[
          "resultados" => $consulta,
          "campos" => ['nomequipo'],
          "titulo" => "Consulta 9 con ActiveRecord",
          "enunciado" => "Listar el nombre de los equipos que tengan más de 4 ciclistas",
          "sql" => "SELECT nomequipo FROM ciclistas GROUP BY nomequipo HAVING COUNT(*) > 4",
        ]);
    }
    
     public function actionConsulta9d() {
        $consulta = new SqlDataProvider ([
          'sql' => 'SELECT nomequipo FROM ciclista GROUP BY nomequipo HAVING count(*) > 4',
          'pagination' => ['pageSize' => 3]
        ]);
        
        return $this->render("resultados",[
          "resultados" => $consulta,
          "campos" => ['nomequipo'],
          "titulo" => "Consulta 9 con DAO",
          "enunciado" => "Listar el nombre de los equipos que tengan más de 4 ciclistas",
          "sql" => "SELECT nomequipo FROM ciclista GROUP BY nomequipo HAVING count(*) > 4",
        ]);
    }
    
    public function actionConsulta10a()
    {
        $consulta = new ActiveDataProvider ([
          'query' => \app\models\Ciclista::find() -> groupBy("nomequipo") -> having("count(*) > 4") -> where("edad between 28 and 32") -> select("nomequipo"),
        ]);
        
        return $this->render("resultados",[
          "resultados" => $consulta,
          "campos" => ['nomequipo'],
          "titulo" => "Consulta 10 con ActiveRecord",
          "enunciado" => "Listar el nombre de los equipos que tengan más de 4 ciclistas cuya edad esté entre 28 y 32",
          "sql" => "SELECT nomequipo FROM ciclistas WHERE edad BETWEEN 28 AND 32 GROUP BY nomequipo HAVING COUNT(*) > 4",
        ]);
    }
    
    public function actionConsulta10d() {
        $total = Yii::$app -> db
                -> createCommand('select count(nomequipo) from ciclista where edad between 28 and 32 group by nomequipo having count(*) > 4')
                -> queryScalar();
        $consulta = new SqlDataProvider ([
          'sql' => 'SELECT nomequipo FROM ciclista WHERE edad BETWEEN 28 AND 32 GROUP BY nomequipo HAVING COUNT(*) > 4',
         
        ]);
        
        return $this->render("resultados",[
          "resultados" => $consulta,
          "campos" => ['nomequipo'],
          "titulo" => "Consulta 10 con DAO",
          "enunciado" => "Listar el nombre de los equipos que tengan más de 4 ciclistas cuya edad esté entre 28 y 32",
          "sql" => "SELECT nomequipo FROM ciclista WHERE edad BETWEEN 28 AND 32 GROUP BY nomequipo HAVING COUNT(*) > 4",
        ]);
    }
    
    public function actionConsulta11a()
    {
        $consulta = new ActiveDataProvider ([
          'query' => \app\models\Etapa::find() -> groupBy("dorsal") -> select("dorsal, count(numetapa) as numeroEtapas"),
          'pagination' => ['pagesize' => 4],
        ]);
        
        return $this->render("resultados",[
          "resultados" => $consulta,
          "campos" => ['dorsal','numeroEtapas'],
          "titulo" => "Consulta 11 con ActiveRecord",
          "enunciado" => "Indícame el número de etapas que ha ganado cada uno de los ciclistas",
          "sql" => "SELECT dorsal, count(numetapa) FROM Etapa GROUP BY dorsal",
        ]);
    }
    
    public function actionConsulta11d() {
        $total = Yii::$app -> db
                -> createCommand('select count(distinct dorsal) from etapa')
                -> queryScalar();
        $consulta = new SqlDataProvider ([
          'sql' => 'SELECT dorsal, count(numetapa) as numeroEtapas FROM Etapa GROUP BY dorsal',
          'totalCount' => $total,
        ]);
        
        return $this->render("resultados",[
          "resultados" => $consulta,
          "campos" => ['dorsal','numeroEtapas'],
          "titulo" => "Consulta 11 con DAO",
          "enunciado" => "Indícame el número de etapas que ha ganado cada uno de los ciclistas",
          "sql" => "SELECT dorsal, count(numetapa) FROM Etapa GROUP BY dorsal",
        ]);
    }
    
     public function actionConsulta12a()
    {
        $consulta = new ActiveDataProvider ([
          'query' => \app\models\Etapa::find() -> groupBy("dorsal") -> having("count(numetapa) > 1") -> select("dorsal"),
        ]);
        
        return $this->render("resultados",[
          "resultados" => $consulta,
          "campos" => ['dorsal'],
          "titulo" => "Consulta 12 con ActiveRecord",
          "enunciado" => "Indícame el dorsal de los ciclistas que hayan ganado más de una etapa",
          "sql" => "SELECT dorsal FROM Etapa GROUP BY dorsal HAVING COUNT(numetapa) > 1",
        ]);
    }
    
    public function actionConsulta12d() {
        $consulta = new SqlDataProvider ([
          'sql' => 'SELECT dorsal FROM Etapa GROUP BY dorsal HAVING COUNT(numetapa) > 1',
        ]);
        
        return $this->render("resultados",[
          "resultados" => $consulta,
          "campos" => ['dorsal'],
          "titulo" => "Consulta 12 con DAO",
          "enunciado" => "Indícame el dorsal de los ciclistas que hayan ganado más de una etapa",
          "sql" => "SELECT dorsal FROM Etapa GROUP BY dorsal HAVING COUNT(numetapa) > 1",
        ]);
    }
}
